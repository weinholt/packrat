;; SPDX-License-Identifier: MIT

(define-library (packrat)
  (export parse-result?
          parse-result-successful?
          parse-result-semantic-value
          parse-result-next
          parse-result-error

          parse-results?
          parse-results-position
          parse-results-base
          parse-results-next

          parse-error?
          parse-error-position
          parse-error-expected
          parse-error-messages
          parse-error->list

          make-parse-position
          parse-position?
          parse-position-file
          parse-position-line
          parse-position-column

          top-parse-position
          update-parse-position
          parse-position->string

          ;;empty-results
          ;;make-results

          make-error-expected
          make-error-message
          make-result
          parse-error->parse-result
          make-expected-result
          make-message-result

          prepend-base
          prepend-semantic-value

          base-generator->results
          results->result

          parse-position>?
          parse-error-empty?
          merge-parse-errors
          merge-result-errors

          parse-results-token-kind
          parse-results-token-value

          packrat-check-base
          packrat-check
          packrat-or
          packrat-unless

          packrat-parser
          packrat-lambda
          packrat-lambda*
          packrat-parse
          try-packrat-parse-pattern

          packrat-port-results
          packrat-string-results
          packrat-list-results

          <- quote ! ^ /)

  (import (scheme base)
          (scheme cxr)
          (scheme lazy)
          (scheme write)
          (only (srfi 1) append-map fold lset-union))

  (begin

    (define gensym
      (let ((last -1))
        (lambda ()
          (set! last (+ last 1))
          (string->symbol
           (string-append "packrat-g" (number->string last))))))

    (define-syntax define-auxiliary-keyword
      (syntax-rules ()
        ((_ keyword)
         (define-syntax keyword
           (syntax-rules ()
             ((_)
              (syntax-error
               "incorrect usage of auxiliary keyword" 'keyword)))))))

    (define-auxiliary-keyword <-)
    (define-auxiliary-keyword !)
    (define-auxiliary-keyword ^)
    ;;(define-auxiliary-keyword /)
    )

  (include "packrat-r5rs.scm"))
