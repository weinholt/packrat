# (packrat)

[![CircleCI](https://circleci.com/gh/weinholt/packrat.svg?style=svg)](https://circleci.com/gh/weinholt/packrat)

This is an R6RS Scheme adaption of Tony Garnock-Jones's packrat parser.

A [manual][manual] is available. More info on packrat can be found on
[The Packrat Parsing and Parsing Expression Grammars Page][packrat].

  [manual]: http://www.lshift.net/~tonyg/packrat.pdf
  [packrat]: http://bford.info/packrat/

The code is based on the historical darcs repository at
http://www.lshift.net/~tonyg/json-scheme/, which was converted to git
with darcs-to-git.
