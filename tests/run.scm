;; These are tests for CHICKEN Scheme

(import scheme)
(cond-expand
  (chicken-4
   (import chicken)
   (use packrat)
   (use srfi-64))
  (chicken-5
   (import packrat)
   (import srfi-64))
  (else
   (error "Unsupported CHICKEN version.")))

(include "test-r5rs.scm")
