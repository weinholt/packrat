(test-begin "expr")

(define expr-parse
  (let ((p ((packrat-parse `((toplevel (e <- expr #f ,(packrat-lambda (e) e)))
			     (expr (/ (a <- mulexp "+"ws b <- expr
					 ,(packrat-lambda (a b) (+ a b)))
				      mulexp))
			     (mulexp (/ (a <- simple "*"ws b <- mulexp
					   ,(packrat-lambda (a b) (* a b)))
					simple))
			     (simple (/ num
					("("ws a <- expr ")"ws
					 ,(packrat-lambda (a) a))))
			     (num ((d <- digit)+ ws
				   ,(packrat-lambda (d) (string->number (list->string d)))))
			     (ws (#\space *))
			     (digit (/: "0123456789"))))
	    'toplevel)))
    (lambda (str)
      (try-packrat-parse-pattern
       p '()
       (packrat-string-results "<str>" str)
       (lambda (bindings result) (values bindings (parse-result-semantic-value result)))
       (lambda (err)
	 (list 'parse-error
	       (parse-position->string (parse-error-position err))
	       (parse-error-expected err)
	       (parse-error-messages err)))))))

(let-values ((x (expr-parse "1 + 2 * 3")))
  (test-equal x '(() 7)))

(let-values ((x (expr-parse "1 + 2 * *")))
  (test-equal x '((parse-error "<str>:1:8" (#\space (one-of "0123456789") "(") ()))))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
