#!/bin/sh
set -eu
cd "$(dirname "$0")"
where=$PWD
cd tests
case "$1" in
    chez*|scheme) "$@" --program test-r6rs.sps ;;
    chibi*)       "$@" -I "$where" test-r7rs.scm ;;
    gosh*)        "$@" -I "$where" test-r7rs.scm ;;
    *)            echo "Unknown Scheme: $1" >&2; exit 2 ;;
esac
